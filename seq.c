#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define sign(x) ((x) == 0 ? (0) : ((x) > 0) ? (1) : (-1))
#define N 1000

int print_result(int32_t numbers[N], int32_t dp[N][2], int32_t indices[N][2]) {
  int zig_zag;
  if (dp[0][0] == dp[0][1]) {
    zig_zag = indices[0][0] < indices[0][1] ? 0 : 1;
  } else {
    zig_zag = dp[0][0] > dp[0][1] ? 0 : 1;
  }

  int i = 0;
  do {
    printf("%d ", numbers[i]);
    i = indices[i][zig_zag];
    zig_zag = !zig_zag;
  }
  while (i != 0);
  printf("\n");

  return 0;
}

int get_sequence(int32_t n, int32_t numbers[N], int32_t dp[N][2], int32_t indices[N][2]) {
  for (int i = 0; i < N; i++) {
    dp[i][0] = dp[i][1] = 1;
    indices[i][0] = indices[i][1] = 0;
  }

  for (int i = n - 1; i >= 0; i--) {
    for (int j = i + 1; j < n; j++) {
      if (numbers[j] < numbers[i] && dp[i][0] <= dp[j][1]) {
        dp[i][0] = dp[j][1] + 1;
        indices[i][0] = j;
      }

      if (numbers[j] > numbers[i] && dp[i][1] <= dp[j][0]) {
        dp[i][1] = dp[j][0] + 1;
        indices[i][1] = j;
      }
    }
  }
}

int main() {
  int32_t n;
  scanf("%d", &n);

  int32_t numbers[N];
  for (int i = 0; i < n; i++) {
    scanf("%d", &numbers[i]);
  }

  int32_t dp[N][2], indices[N][2];
  get_sequence(n, numbers, dp, indices);

  print_result(numbers, dp, indices);

  return 0;
}
